import json
import requests
from flask import Flask, render_template, request, jsonify
import overpy
from shapely.geometry import shape
import overpy
import pandas as pd
import geopandas as gpd
import numpy as np
import geopy.distance
import csv

app = Flask(__name__)
dominos_locations=[]
@app.route('/')
def index():
    return render_template('map.html')

@app.route('/isochrone', methods=['POST'])
def isochrone():
    lat = request.json['lat']
    lon = request.json['lon']
    time=(int(request.json['sec'])*60)
    
    body = {"locations":[[lon, lat]], "range":[time],"range_type":"time"}
    headers = {
    'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
    'Authorization': '5b3ce3597851110001cf6248ba2a1839cb0c4cea91c33ae89007ec1d',
    'Content-Type': 'application/json; charset=utf-8'
    }
    call = requests.post('https://api.openrouteservice.org/v2/isochrones/foot-walking', json=body, headers=headers)

    data = json.loads(call.text)
    return jsonify(data)
@app.route('/pizza-restaurants', methods=['GET', 'POST'])
def pizza_restaurants():
    if request.method == 'POST':
        api = overpy.Overpass()

        # Define the Overpass query to search for restaurants selling pizza in Bengaluru
        overpass_query = """
        area[name="Bengaluru"]->.searchArea;
        node(area.searchArea)[cuisine=pizza];
        out;
        """

        # Make the request to Overpass API
        result = api.query(overpass_query)

        restaurants = []
        for node in result.nodes:
            
            restaurant = {
                'name':node.tags.get('name'),
                'lat': node.lat,
                'lon': node.lon
            }
            restaurants.append(restaurant)
        global dominos_locations
        dominos_locations=[restaurant for restaurant in restaurants if restaurant['name']=="Domino's"]

        return jsonify(restaurants)

    return "This route only supports POST requests."
@app.route('/flood-prone-areas', methods=['GET','POST'])
def get_flood_prone_nodes():

    api = overpy.Overpass()
    query = '''
        [out:json];
        area[name="Bengaluru"]->.searchArea;
        (
            node(area.searchArea)["natural"="wetland"];
            node(area.searchArea)["waterway"="riverbank"];
            node(area.searchArea)["landuse"="reservoir"];
            node(area.searchArea)["waterway"="river"];
        );
        out body;
        '''
    result = api.query(query)
    
    nodes = []
    for node in result.nodes:
        nodes.append({
            'id': node.id,
            'lat': node.lat,
            'lon': node.lon
        })
    
    return jsonify(nodes)

@app.route('/geojson')
def get_geojson():
    return app.send_static_file('BBMP.GeoJSON')


@app.route('/GET-malls', methods=['GET','POST'])
def get_malls():
    api = overpy.Overpass()
    query = '''
   [out:json];
area[name="Bengaluru"]->.searchArea;
(
node["shop"="mall"](area.searchArea);
  way["shop"="mall"](area.searchArea);
  relation["shop"="mall"](area.searchArea);
  
);

out center;


'''

    result = api.query(query)
    

# Load the GeoJSON file
    with open('static/uni.geojson', encoding='utf-8') as file:
        data = json.load(file)


# Iterate over the features and extract the points
    nodes=[]
    points = []
    for feature in data['features']:
        name=feature['properties']['name']
    
        geometry = feature['geometry']
        if geometry['type'] == 'Point':
            nodes.append({
                'name':name,
                'lat':geometry['coordinates'][1],
                'lon':geometry['coordinates'][0]
            })
        
            


    
    for node in result.nodes:
        nodes.append({
            'name': node.tags['name'],
            'lat': node.lat,
            'lon': node.lon
        })
    print(nodes)
    
    return jsonify(nodes)   
@app.route('/dominos-isochrones', methods=['GET','POST'])
def get_dominos_isochrones():
    locations=request.json['locations']
    # Retrieve the location data from the request
    print(locations)
    batch_size = 5
    
    # Calculate the number of batches needed
    num_batches = len(locations) // batch_size + 1
    
    isochrones = []
    
    # Send requests for each batch of locations
    for i in range(num_batches):
        start_idx = i * batch_size
        end_idx = start_idx + batch_size
        
        # Extract the current batch of locations
        batch_locations = locations[start_idx:end_idx]
        
        batch_body = {
            'locations': batch_locations,
            'range': [900]
        }
        
        headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
            'Authorization': '5b3ce3597851110001cf6248ba2a1839cb0c4cea91c33ae89007ec1d',
            'Content-Type': 'application/json; charset=utf-8'
        }
        
        response = requests.post('https://api.openrouteservice.org/v2/isochrones/foot-walking', json=batch_body, headers=headers)
        print(response)
        if response.status_code == 200:
            isochrones.append(response.json())
        else:
            return jsonify({'error': response.json()}), response.status_code
weights = None

@app.route('/GET-Analytics', methods=['POST'])
def SetWeights():
    global weights
    weights = request.json
    return "OK"  # Return a response to acknowledge the successful POST request

@app.route('/GET-Analytics',methods=['GET'])

def Analytics():
        global weights
        params1=weights['weight1']
        params2=weights['weight2']
        params3=weights['weight3']
        params4=weights['weight4']
        params5=weights['weight5']
        params6=weights['weight6']
        params7=weights['weight7']


        api=overpy.Overpass()
        result=api.query("""
        area[name="Bengaluru"]->.searchArea;
        (
        node["cuisine"="pizza"](area.searchArea);
        
        );
        out geom;""")
        dominos_rows=[]
        other_rows=[]
        for restaurant in result.nodes:
            name=restaurant.tags.get('name')
            lat=restaurant.lat
            lon=restaurant.lon
            if name=="Domino's":
                dominos_rows.append({'name':name,'lat':lat,'lon':lon})
            else:
                other_rows.append({'name':name,'lat':lat,'lon':lon})
        dominos_df=pd.DataFrame(dominos_rows)
        other_df=pd.DataFrame(other_rows)
        gdf=gpd.read_file('static/BBMP.GeoJSON')    #Change the file address
        new_gdf=gdf[['WARD_NO','WARD_NAME','POP_TOTAL','AREA_SQ_KM','LAT','LON','geometry']]
        new_gdf.loc[:,'DENSITY']=new_gdf['POP_TOTAL']//new_gdf['AREA_SQ_KM']
        new_gdf=gdf[['WARD_NO','WARD_NAME','POP_TOTAL','AREA_SQ_KM','LAT','LON','geometry']]
        new_gdf.loc[:,'DENSITY']=new_gdf['POP_TOTAL']//new_gdf['AREA_SQ_KM']
        from shapely.geometry import Point
        dominos_df['geometry']=[Point(xy) for xy in zip(dominos_df.lon,dominos_df.lat)]
        gdf_points1=gpd.GeoDataFrame(dominos_df,geometry='geometry')
        for index,row in new_gdf.iterrows():
            ward_number=row["WARD_NO"]
            density=row['DENSITY']
            ward_boundary=row["geometry"]
            points_in_ward=gdf_points1[gdf_points1.within(ward_boundary)]
            gdf_points1.loc[points_in_ward.index,'WARD_NO']=ward_number
        gdf_points1.dropna()
        from shapely.geometry import Point
        other_df['geometry']=[Point(xy) for xy in zip(other_df.lon,other_df.lat)]
        gdf_points2=gpd.GeoDataFrame(other_df,geometry='geometry')
        for index,row in new_gdf.iterrows():
            ward_number=row["WARD_NO"]
            density=row['DENSITY']
            ward_boundary=row["geometry"]
            points_in_ward=gdf_points2[gdf_points2.within(ward_boundary)]
            gdf_points2.loc[points_in_ward.index,'WARD_NO']=ward_number
        gdf_points2.dropna()

        api=overpy.Overpass()

        shopping_Mall=api.query("""
        [out:json];
        area[name="Bengaluru"]->.searchArea;
        (
        node["shop"="mall"](area.searchArea);
        way["shop"="mall"](area.searchArea);
        relation["shop"="mall"](area.searchArea);
        );
        out center;

        """)
        shopping_rows=[]
        for malls in shopping_Mall.nodes:
            name=malls.tags.get('name')
            lat=malls.lat
            lon=malls.lon
            shopping_rows.append({'name':name,'lat':lat,'lon':lon})

        malls_df=pd.DataFrame(shopping_rows)


        from shapely.geometry import Point
        malls_df['geometry']=[Point(xy) for xy in zip(malls_df.lon,malls_df.lat)]
        malls_df=gpd.GeoDataFrame(malls_df,geometry='geometry')
        
        for index,row in new_gdf.iterrows():
            ward_number=row["WARD_NO"]
            density=row['DENSITY']
            ward_boundary=row["geometry"]
            points_in_ward=malls_df[malls_df.within(ward_boundary)]
            malls_df.loc[points_in_ward.index,'WARD_NO']=ward_number
        malls_df.dropna()

        min_distances = {}

    # loop over each ward
        for i, ward in new_gdf.iterrows():
            ward_coords = (ward['LAT'], ward['LON'])
            ward_no = ward['WARD_NO']
        
    
        # check if a Domino's store is present in the same ward
            in_same_ward = gdf_points1['WARD_NO'].isin([ward_no]).any()
        
        
    
            if in_same_ward:
                # if a Domino's store is in the same ward, set the minimum distance to 0
                min_distance = 0
        
            else:
            # if no Domino's store is in the same ward, calculate the minimum distance from the ward to all the Domino's stores
                dominos_coords = [(lat, lon) for lat, lon in zip(gdf_points1['lat'], gdf_points1['lon'])]
                distances = [geopy.distance.distance(ward_coords, domino_coords).km for domino_coords in dominos_coords]
                min_distance = min(distances) if distances else None
    
            min_distances[ward_no] = min_distance

        minimum_distance=pd.DataFrame(list(min_distances.items()),columns=['WARD_NO','DISTANCE'])
        merged_dominos=pd.merge(minimum_distance,new_gdf,on='WARD_NO',how="right")
        merged_dominos['DISTANCE']=merged_dominos['DISTANCE'].fillna(0)
        merged_dominos=merged_dominos[['WARD_NO','DISTANCE']]
        api=overpy.Overpass()

        flood_prone=api.query("""
        area[name="Bengaluru"]->.searchArea;
        (
        way["waterway"="riverbank"](area.searchArea);
        way["waterway"="streambank"](area.searchArea);
        relation["waterway"="riverbank"](area.searchArea);
        relation["waterway"="streambank"](area.searchArea);
        way["natural"="wetland"](area.searchArea);
        relation["natural"="wetland"](area.searchArea);
        );
        out geom;
        >;
        out skel;
        """)
        from shapely.geometry import Point,MultiPolygon
        prone=[]
        for index,row in new_gdf.iterrows():
            ward_number=row["WARD_NO"]
            ward_boundary=row["geometry"]
            ward_polygon= MultiPolygon(ward_boundary)
            for node in flood_prone.nodes:
                node_point=Point(node.lon,node.lat)
                if node_point.within(ward_polygon):
                    prone.append({"WARD_NO":ward_number})
                    break
        flood_prone_df=pd.DataFrame(prone)
        score_df=new_gdf.drop('geometry',axis=1)
        score_df['flood_prone']=np.where(gdf['WARD_NO'].isin(flood_prone_df["WARD_NO"]),1,0)
        mall_count=malls_df.groupby('WARD_NO').size().reset_index(name='mall_count')
        score_df=pd.merge(score_df,mall_count,left_on="WARD_NO",right_on="WARD_NO",how="left")
        score_df['mall_count']=score_df['mall_count'].fillna(0).astype(int)
        dominos_count=gdf_points1.groupby("WARD_NO").size().reset_index(name='dominos_count')
        score_df=pd.merge(score_df,dominos_count,left_on="WARD_NO",right_on="WARD_NO",how="left")
        score_df['dominos_count']=score_df['dominos_count'].fillna(0).astype(int)
        other_count=gdf_points2.groupby("WARD_NO").size().reset_index(name="competitors_count")
        score_df=pd.merge(score_df,other_count,left_on="WARD_NO",right_on="WARD_NO",how="left")
        score_df['competitors_count']=score_df['competitors_count'].fillna(0).astype(int)
        score_df['DENSITY_PROP']=score_df['DENSITY']/max(score_df['DENSITY'])
        DF=pd.read_json('static/restaurants.json')
        names=[]
        latitudes=[]
        longitudes=[]
        for feature in DF['features']:
            try:
                name=feature['properties']['name']
                longitude,latitude=feature['geometry']['coordinates']
                names.append(name)
                latitudes.append(latitude)
                longitudes.append(longitude)
            except KeyError:
                pass
        DF_1=pd.DataFrame({'name':names,'latitude':latitudes,'longitude':longitudes})
        DF_1.dropna(inplace=True)
        from shapely.geometry import Point
        DF_1['geometry']=[Point(xy) for xy in zip(DF_1.longitude,DF_1.latitude)]
        DF_points=gpd.GeoDataFrame(DF_1,geometry='geometry')
        for index,row in new_gdf.iterrows():
            ward_number=row["WARD_NO"]
            density=row['DENSITY']
            ward_boundary=row["geometry"]
            points_in_ward=DF_points[DF_points.within(ward_boundary)]
            DF_points.loc[points_in_ward.index,'ward_number']=ward_number
        grouped_df=DF_points.groupby('ward_number')['name'].count().reset_index()
        grouped_df=grouped_df.rename(columns={'ward_number':"WARD_NO",'name':"RESTAURANT_COUNT"})
        score_df=pd.merge(grouped_df,score_df,on="WARD_NO",how="right")
        score_df=score_df.fillna(0)
        score_df['RESTAURANT_PROP']=score_df['RESTAURANT_COUNT']/max(score_df['RESTAURANT_COUNT'])
        wealth=pd.read_csv('static/wealth.csv')
        score_df=pd.merge(wealth,score_df,on="WARD_NO",how='right')
        score_df['WEALTH']=score_df['FLAT_COST']/max(score_df['FLAT_COST'])

        score_df=pd.merge(merged_dominos,score_df,on="WARD_NO",how="right")
        score_1=[]
        for i,row in score_df.iterrows():    
            dominos_count = row["dominos_count"]
            competitors_count = row['competitors_count']
            score1,score2,score3,score4,score5=1,2,3,4,5
            distance=row['DISTANCE']
            distance_score=0
            if dominos_count > 2 and competitors_count < dominos_count:
                score = score1
            elif dominos_count > 2 and competitors_count == dominos_count:
                score = score2
            elif dominos_count > 2 and competitors_count > dominos_count:
                score = score1
            elif dominos_count == 1 and competitors_count == 0:
                score = score2
            elif dominos_count == 1 and competitors_count == dominos_count:
                score = score3
            elif dominos_count == 1 and competitors_count > dominos_count:
                score = score2
            elif dominos_count == 0 and competitors_count == 0:
                score = score5
            elif dominos_count == 0 and competitors_count > dominos_count:
                score = score4
            if distance == 0:
                distance_score=0
            elif 0 < distance < 1.103774:
                distance_score=0.1 * distance
            elif 1.103774 < distance < 1.802921:
                distance_score=0.2 * distance
            elif 1.802921 < distance < 2.996105:
                distance_score= 0.3 * distance
            elif distance > 2.996105:
                distance_score=0.4 * distance
            if row["mall_count"] > 0:
                score += (0.5) * 1
            if row["flood_prone"] == 1:
                score -= 0.5
            score_1.append(float(params1)*score+(float(params2)*distance_score)+(float(params3))*row['DENSITY_PROP']+(float(params4)*row['RESTAURANT_PROP'])+(float(params5))*row['WEALTH'] - (float(params6)* row['flood_prone']) + (float(params7)*row['mall_count'])) 

        score_df['score']=score_1
        score_df=score_df.sort_values(['score'],ascending=False)
        desired_columns = ['WARD_NO', 'WARD_NAME', 'dominos_count', 'competitors_count', 'flood_prone', 'mall_count','WEALTH','score']
     
        score_df_filtered = score_df[desired_columns]
        print(score_df_filtered)

        
        


        score_df_filtered=score_df_filtered.head(15)
        score_dict = score_df_filtered.to_dict(orient='records')


        return jsonify(score_dict)
@app.route('/get-wealth-data')
def get_wealth_data():
    wealth_data = []

    with open('static/wealth.csv', 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            
            wealth_entry = {
                'ward_no': row['WARD_NO'],
                'wealth': float(row['FLAT_COST'])
            }
            wealth_data.append(wealth_entry)

    return jsonify(wealth_data)








   



if __name__ == '__main__':
    app.run()
